**Användarmanual**

Det här är ett sida där du som användare kan skicka krypterade meddelande till andra användare
För att använda sig av CryptoChat så måste man först ta ner det till sin dator antingen via en git client eller via en zippad fil.

För att cryptochat ska compilera korrekt bör du ta ner tillhörande bibliotek:

https://bitbucket.org/trollungen/cryptochat.dbservice

https://bitbucket.org/trollungen/cryptoservice

Öppna biblioteken i visual studio och bygg.
I bin mappen finns .dll filen som du ska adda till cryptochat

cryptochat.dbservice använder sig av code first.
Just nu är cryptochat web.config fil inställd på en local databas på min dator. Så glöm inte att editera 'connectionstring' till din egen