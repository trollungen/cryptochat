﻿
function Connect() {

    var chatHub = $.connection.chatHub;

    registerClientMethods(chatHub);

    $.connection.hub.start().done(function () {

        chatHub.server.connect();

    });

    $('.logoutbtn').on('click', function (e) {
        e.preventDefault();

        chatHub.connection.stop();
        $.ajax({
            url: "Account/Logout",
            type: "POST",
        }).done(function () {
            window.location.reload();
        });
    });
}

