﻿function registerClientMethods(chatHub) {

    chatHub.client.onConnected = function (id, userName, allUsers) {


        $('#currentUserId').val(id);
        $('#currentUsername').val(userName);

        for (i = 0; i < allUsers.length; i++) {

            AddUser(chatHub, allUsers[i].ConnectionId, allUsers[i].OnlineUserName);
        }
    }

    chatHub.client.onNewUserConnected = function (id, name) {

        AddUser(chatHub, id, name);
    }

    chatHub.client.onUserDisconnected = function (id, userName) {
        $('#' + id).remove();

        var ctrId = 'private_' + id;
        $('#' + ctrId).remove();


        var disc = $('<div class="disconnect">"' + userName + '" logged off.</div>');

        $(disc).hide();
        $('#divusers').prepend(disc);
        $(disc).fadeIn(200).delay(2000).fadeOut(200);

    }

    chatHub.client.onUserReConnected = function () {

        chatHub.server.connect();
    }

    chatHub.client.sendPrivateMessage = function (windowId, fromUserName, message) {

        var ctrId = 'private_' + windowId;


        if ($('#' + ctrId).length == 0) {

            createPrivateChatWindow(chatHub, windowId, ctrId, fromUserName);

        }

        $('#' + ctrId).find('#divMessage').append('<div class="message"><span class="userName">' + fromUserName + '</span>: ' + message + '</div>');

        var height = $('#' + ctrId).find('#divMessage')[0].scrollHeight;
        $('#' + ctrId).find('#divMessage').scrollTop(height);

    }

}