/// <autosync enabled="true" />
/// <reference path="bootstrap.js" />
/// <reference path="jquery-2.1.3.js" />
/// <reference path="jquery-ui-1.11.4.js" />
/// <reference path="jquery.signalr-2.2.0.js" />
/// <reference path="jquery.validate.js" />
/// <reference path="jquery.validate.unobtrusive.js" />
/// <reference path="modernizr-2.6.2.js" />
/// <reference path="respond.js" />
/// <reference path="menunav/jexpand.js" />
/// <reference path="menunav/message.js" />
/// <reference path="menunav/simple-expand.js" />
/// <reference path="chat/connect-disconnect.js" />
/// <reference path="chat/userlist.js" />
/// <reference path="chat/build-chat.js" />
/// <reference path="chat/client-functions.js" />
