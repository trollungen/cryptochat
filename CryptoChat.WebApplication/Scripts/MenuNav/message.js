﻿
$('.message').on('click', function (event)
{
    event.preventDefault();

    var $contentdetails = $('#contentdetails'),
        url = $(this).data('url');

    $.get(url, function (data) {
        $contentdetails.replaceWith(data);
    });
});