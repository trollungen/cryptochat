﻿using Owin;
using Microsoft.Owin;
using Microsoft.AspNet.SignalR;
using CryptoChat.WebApplication.App_Start.DependencyResolvers;
[assembly: OwinStartup(typeof(CryptoChat.Startup))]
namespace CryptoChat
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            //var idProvider = new CustomUserIdProvider();
            //GlobalHost.DependencyResolver.Register(typeof(IUserIdProvider), () => idProvider);

            app.MapSignalR();
        }
    }
}