﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Web;
using System.Web.Mvc;
using CryptoMessanger.DbService;
using CryptoService;

namespace CryptoChat.WebApplication.Controllers
{
    [Authorize]
    public class MessageController : Controller
    {
        private readonly IMessageService _messageService;

        public MessageController(IMessageService messageService)
        {
            _messageService = messageService;
        }

        public ActionResult SendMessage()
        {
            List<UserModel> getAllUsers = _messageService.AllUsersExceptCurrent(User.Identity.Name);

            ViewBag.Members = getAllUsers;

            ViewBag.messageList = _messageService.GetListOfMessage(User.Identity.Name);

            MessangerModel messanger = new MessangerModel();

            return View("_SendMessage", messanger);
        }

        [HttpPost]
        public ActionResult SendMessage(MessangerModel msg)
        {
            if (msg.IsEncrypted)
            {
                var jsonstring = SendMessageEncrypted(msg);
                if (!String.IsNullOrEmpty(jsonstring))
                    msg.Message = jsonstring;
            }

            if (msg != null)
            {
                msg.SenderUsername = User.Identity.Name;
                var sendmessage = _messageService.SendMessage(msg);
                if (sendmessage.MessageError != null)
                {
                    ViewBag.MessageResult = sendmessage.MessageError;
                    ViewBag.MessageException = sendmessage.Exception;
                }
                return RedirectToAction("Index", "Home");
            }
            return RedirectToAction("Index", "Home");
        }

        private string SendMessageEncrypted(MessangerModel message)
        {
            var cryptoservice = new CryptographyService();
            var publicKey = _messageService.GetUserPublicKey(User.Identity.Name);

            // Get PublicKey from Reciever
            String encryptionData = cryptoservice.GetDataForEncryption();
            PublicKeyResponse recieverData = JsonConvert.DeserializeObject<PublicKeyResponse>(encryptionData);
            
            // Encrypt my message with my publickey
            String encryptedMessage = cryptoservice.EncryptMessageAES(message.Message, publicKey);
            EncryptedMessageResponse encryptedMessageData = JsonConvert.DeserializeObject<EncryptedMessageResponse>(encryptedMessage);

            // Encrypt my publickey with recievers publickey
            String myEncryptPublicKey = cryptoservice.EncryptPublicKey(publicKey, recieverData.PublicKey);

            // Send encryptedmessagedata, my encrypted publickey and keyid to get the completed encryptedMessage back
            return cryptoservice.EncryptFinalMessage(recieverData.KeyId, myEncryptPublicKey, encryptedMessageData.Ciphertext, encryptedMessageData.InitialisationVector);
        }

        public JsonResult DecryptMessage(string message)
        {
            var cryptoservice = new CryptographyService();

            var result = cryptoservice.DecryptMessage(message);
            _messageService.DeleteMessage(message);

            return Json(result);
        }

    }
}