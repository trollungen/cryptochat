﻿using CryptoChat.WebApplication.Hubs;
using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using CryptoMessanger.DbService;

namespace CryptoChat.WebApplication.Controllers
{
    public class AccountController : Controller
    {
        private readonly IAuthenticationService _authenticationService;

        public AccountController(IAuthenticationService authenticationService)
        {
            _authenticationService = authenticationService;
        }

        public ActionResult Login()
        {
            return PartialView();
        }

        [HttpPost]
        public ActionResult Login(string username, string password)
        {
            
            if (ModelState.IsValid)
            {
                AuthenticationResponse trytoLogin = _authenticationService.TryToLogin(username, password);
                if (trytoLogin.IsValid)
                {
                    FormsAuthentication.SetAuthCookie(username, true);
                    var hub = GlobalHost.ConnectionManager.GetHubContext<ChatHub>();
                    return RedirectToAction("Index", "Home");
                }
                ViewBag.ErrorMessage = trytoLogin.AuthenticationError;

            }
            return PartialView();
        }

        public ActionResult CreateUser()
        {
            return PartialView();
        }

        [HttpPost]
        public ActionResult CreateUser(string username, string password)
        {
            if (ModelState.IsValid)
            {
                var register = _authenticationService.TryToRegister(username, password);
                if (register.IsValid)
                {
                    return RedirectToAction("Login", "Account");
                }
                ModelState.AddModelError("", register.AuthenticationError);
            }
            return PartialView();
        }

        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Login");
        }
    }
}