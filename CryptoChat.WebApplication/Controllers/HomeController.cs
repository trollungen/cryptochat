﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Collections;
using System.IO;
using Microsoft.AspNet.SignalR;
using CryptoChat.WebApplication.Hubs;
using System.Web.Security;
using CryptoMessanger.DbService;

namespace CryptoChat.WebApplication.Controllers
{
    public class HomeController : Controller
    {
        private readonly IConnectionService _connectionService;

        public HomeController(IConnectionService connectionService)
        {
            _connectionService = connectionService;
        }

        public ActionResult Index()
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("SendMessage", "Message");
            }
            return RedirectToAction("Login", "Account");
        }

        private List<OnlineUser> GetOnlineList()
        {
            List<string> userlist = new List<string>();

            return _connectionService.GetConnectedUserList();
        }

        public void LogOut()
        {
            FormsAuthentication.SignOut();
            RedirectToAction("Login", "Account");
        }

    }
}