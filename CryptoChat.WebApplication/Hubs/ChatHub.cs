﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using System.Threading.Tasks;
using System.Web.Security;
using CryptoMessanger.DbService;

namespace CryptoChat.WebApplication.Hubs
{
    public class ChatHub : Hub
    {
        ConnectionService connectionService = new ConnectionService();

        public void Connect()
        {
            var name = Context.User.Identity.Name;
            var connectionid = Context.ConnectionId;

            var connectuser = connectionService.UpdateConnectionOnUser(name, connectionid, true);
            var onlinelist = connectionService.GetConnectedUserList();
            
            Clients.Caller.onConnected(connectionid, name, onlinelist);

            Clients.AllExcept(connectionid).onNewUserConnected(connectionid, name);

        }

        public void SendPrivateMessage(string touserId, string message)
        {
            var fromuserId = Context.ConnectionId;

            var connecteduser = connectionService.GetConnectedUserList();

            var touser = connecteduser.FirstOrDefault(x => x.ConnectionId == touserId);
            var fromuser = connecteduser.FirstOrDefault(x => x.ConnectionId == fromuserId);

            if (touser != null && fromuser != null)
            {

                Clients.Client(touserId).sendPrivateMessage(fromuserId, fromuser.OnlineUserName, message);
                Clients.Caller.sendPrivateMessage(touserId, fromuser.OnlineUserName, message);
            }

        }

        public override Task OnReconnected()
        {
            return base.OnReconnected();
        }

        public override Task OnDisconnected(bool stopCalled)
        {

            var allconnecteduser = connectionService.GetConnectedUserList();
            var connecteduser = allconnecteduser.FirstOrDefault(x => x.ConnectionId == Context.ConnectionId);
            // om user finns i allconnecteduser men connecteduser är null, så har usern reconnectat och måste regisreras igen.
            if (connecteduser != null)
            {
                var response = connectionService.UpdateConnectionOnUser(connecteduser.OnlineUserName, null, false);
                if (!response.ConnectionStatus)
                {
                    var id = response.UserID;
                    Clients.All.onUserDisconnected(id, connecteduser.OnlineUserName);
                }
            }

            return base.OnDisconnected(stopCalled);
            ////return Clients.All.onUserReConnected();
        }
    }
}